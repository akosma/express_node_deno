const express = require('express')
const app = express()
const port = 3000

app.use('/static', express.static('public', { fallthrough: false, index: false }))

app.get("/", (req, res) => {
  res.send(`<html>
  <h1>Test in JavaScript</h1>
  <link rel="stylesheet" href="static/css/test.css">
</html>`)
})

app.listen(port, () => {
  console.log(`JavaScript app listening on port ${port}`)
})
