// @deno-types="npm:@types/express@4.17.15"
import express from "npm:express@4.18.2"
const app = express()
const port = 3001

app.use('/static', express.static('public', { fallthrough: false, index: false }))

app.get("/", (req, res) => {
  res.send(`<html>
  <h1>Test in TypeScript</h1>
  <link rel="stylesheet" href="static/css/test.css">
</html>`)
})

app.listen(port, () => {
  console.log(`TypeScript app listening on port ${port}`)
})
